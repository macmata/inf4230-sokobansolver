/* INF4230 - Intelligence artificielle
 * UQAM / Département d'informatique
 * Automne 2014 / TP1 - Algorithme A*
 * http://ericbeaudry.ca/INF4230/tp1/
 */
package sokoban;

import java.util.ArrayList;

/**
 * Représente un goalInProgress.
 */
public class But implements astar.But, astar.Heuristique {


    public ArrayList<Noeud> buts = new ArrayList<>();

    public void addBut(Noeud noeud) {
        buts.add(noeud);
    }

    @Override
    public boolean butSatisfait(astar.Etat e) {

        int goal = 0;
        EtatSokoban etat = (EtatSokoban) e;
        if (etat.butList == null){
            etat.butList = buts;
        }
        for (Noeud n: etat.butList){
            for (Noeud n1: etat.boxeList){
                if (n1.equals(n)){
                    goal++;
                }
            }
            if(goal==0)return false;
        }
        return goal >= etat.boxeList.size();
    }

    @Override
    public double estimerCoutRestant(astar.Etat e, astar.But b) {
        EtatSokoban etat = (EtatSokoban) e;
        int box =0;
        int player=0;
        if (etat.butList != null){
            for (Noeud n: etat.boxeList){
                player += (Math.abs(n.x - etat.player.x) + Math.abs(n.y - etat.player.y));
                for (Noeud but:etat.butList){
                    box += Math.abs(n.x - but.x) + Math.abs(n.y - but.y);
                }
            }
        }
        return (0.5*box)+(0.3*player);
    }
}
