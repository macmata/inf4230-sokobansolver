/* INF4230 - Intelligence artificielle
 * UQAM / Département d'informatique
 * Automne 2014 / TP1 - Algorithme A*
 * http://ericbeaudry.ca/INF4230/tp1/
 */
package sokoban;


import astar.Etat;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Dans le jeu de sokoban, le «Monde» est une «Grille».
 */
public class Grille implements astar.Monde, astar.But {

    protected Noeud[][] grille;

    int width;
    int height;

    public Grille(int x, int y) {
        width = x;
        height = y;
        grille = new Noeud[x][y];
    }

    public void addNoeud(Noeud noeud) {
        grille[noeud.x][noeud.y] = noeud;
    }

    public void addAll(ArrayList<Noeud> list) {
        for (Noeud noeud : list) {
            this.addNoeud(noeud);
        }
    }

    @Override
    public List<astar.Action> getActions(astar.Etat e) {
        return getPlayerMove(e);
    }


    private List<astar.Action> getPlayerMove(astar.Etat e) {
        ArrayList<astar.Action> listActions = new ArrayList<>();
        EtatSokoban etat = (EtatSokoban) e;
        Noeud n = etat.player;
            if (n.x + 1 < width) {
                Noeud east = this.grille[n.x + 1][n.y];
                if (!east.block) {
                    listActions.add(new ActionDeplacement(east, "E"));
                }
            }
            if (n.x - 1 >= 0) {
                Noeud west = this.grille[n.x - 1][n.y];
                if (!west.block) {
                    listActions.add(new ActionDeplacement(west, "W"));
                }
            }
            if (n.y + 1 < width) {
                Noeud south = this.grille[n.x][n.y + 1];
                if (!south.block) {
                    listActions.add(new ActionDeplacement(south, "S"));
                }
            }
            if (n.y - 1 >= 0) {
                Noeud north = this.grille[n.x][n.y - 1];
                if (!north.block) {
                    listActions.add(new ActionDeplacement(north, "N"));
                }
            }
        return listActions;
    }

    @Override
    public astar.Etat executer(astar.Etat e, astar.Action a) {
        EtatSokoban etat = (EtatSokoban) e;
        ActionDeplacement move = (ActionDeplacement) a;
        EtatSokoban newState = etat.clone();
        int collision = 0;

        for (int i = 0; i < newState.boxeList.size(); i++) {
            Noeud box = newState.boxeList.get(i);
            if (box.equals(move.prochain)) {
                Noeud nextBoxPosition = pushBox(box, move);

                if (!isCollision(etat, nextBoxPosition) && isMoveValid(box, move)) {
                    newState.boxeList.set(i, nextBoxPosition);
                } else {
                    if (box.equals(move.prochain)) {
                        collision++;
                    }
                }
            }
        }
        if (collision == 0) {
            newState.player.setXY(move.prochain.x, move.prochain.y);
        }

        return newState;
    }

    public Noeud pushBox(Noeud noeud, ActionDeplacement positionAction) {
        Noeud newNoeud = null;
        switch (positionAction.nom) {
            case "E":
                newNoeud = new Noeud(noeud.x + 1, noeud.y);
                break;
            case "W":
                newNoeud = new Noeud(noeud.x - 1, noeud.y);
                break;
            case "S":
                newNoeud = new Noeud(noeud.x, noeud.y + 1);
                break;
            case "N":
                newNoeud = new Noeud(noeud.x, noeud.y - 1);
                break;
        }
        return newNoeud;
    }

    public Boolean isMoveValid(Noeud n, ActionDeplacement positionAction) {
        switch (positionAction.nom) {
            case "E":
                if (n.x + 1 < width) {
                    Noeud moveEst = this.grille[n.x + 1][n.y];
                    return !(moveEst.block || isPositionDeathLock(moveEst));
                } else {
                    return false;
                }

            case "W":
                if (n.x - 1 >= 0) {
                    Noeud moveOuest = this.grille[n.x - 1][n.y];
                    return !(moveOuest.block || isPositionDeathLock(moveOuest));
                } else {
                    return false;
                }

            case "S":
                if (n.y + 1 < width) {
                    Noeud movesud = this.grille[n.x][n.y + 1];
                    return !(movesud.block || isPositionDeathLock(movesud));
                } else {
                    return false;
                }
            case "N":
                if (n.y - 1 >= 0) {
                    Noeud moveNord = this.grille[n.x][n.y - 1];
                    return !(moveNord.block || isPositionDeathLock(moveNord));
                } else {
                    return false;
                }
        }
        return false;
    }


    private boolean isPositionDeathLock(Noeud n) {
        int deathLock = 0;
        if (n.x + 1 < width && n.x - 1 > 0 &&
                n.y + 1 < height && n.y - 1 > 0) {
            Noeud est = this.grille[n.x + 1][n.y];
            Noeud north = this.grille[n.x][n.y - 1];
            Noeud west = this.grille[n.x - 1][n.y];
            Noeud south = this.grille[n.x][n.y + 1];
            if (est.block && north.block) deathLock++;
            if (north.block && west.block) deathLock++;
            if (south.block && est.block) deathLock++;
            if (south.block && west.block) deathLock++;

        } else {
            deathLock++;
        }

        return deathLock >= 2;
    }

    public boolean isCollision(Etat e, Noeud n) {
        EtatSokoban etat = (EtatSokoban) e;
        int collision = 0;
        for (Noeud n1 : etat.boxeList) {
            if (n1 != n) {
                if (n1.equals(n)) {
                    collision++;
                }
            }
        }
        return collision > 0;
    }

    @Override
    public boolean butSatisfait(astar.Etat e) {
        EtatSokoban etat = (EtatSokoban) e;
        return false;
    }

}
