package sokoban;

public class Noeud {
    protected boolean block;
    protected int x;
    protected int y;

    public Noeud(int x, int y) {
        this(x, y, false);
    }

    public Noeud(int x, int y, boolean block) {
        this.y = y;
        this.x = x;
        this.block = block;
    }

    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public boolean equals(Object obj) {
        Noeud n1 = (Noeud) obj;
        return n1.x == this.x && n1.y == this.y;
    }

    @Override
    public int hashCode() {
        int result = (block ? 1 : 0);
        result = 31 * result + x;
        result = 31 * result + y;
        return result;
    }

    @Override
    public String toString() {
        return "Noeud{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
