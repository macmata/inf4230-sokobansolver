/* INF4230 - Intelligence artificielle
 * UQAM / Département d'informatique
 * Automne 2014 / TP1 - Algorithme A*
 * http://ericbeaudry.ca/INF4230/tp1/
 */

package sokoban;

import astar.Etat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

/**
 * Représente un état d'un monde du jeu Sokoban.
 */

public class EtatSokoban extends Etat {
    Noeud player;
    ArrayList<Noeud> butList;
    ArrayList<Noeud> boxeList;


    public EtatSokoban() {
        boxeList = new ArrayList<>();
    }

    public void addBox(Noeud boxe) {
        this.boxeList.add(boxe);
    }


    public void debug() {

    }

    @Override
    public EtatSokoban clone() {
        EtatSokoban newState = new EtatSokoban();
        newState.boxeList = new ArrayList<>();
        newState.player = new Noeud(this.player.x, this.player.y);
        newState.butList =this.butList;
        for (Noeud n : this.boxeList) {
            newState.boxeList.add(new Noeud(n.x, n.y));
        }
        return newState;
    }

    @Override
    public int hashCode() {
        int result = player != null ? player.hashCode() : 0;
        result = 31 * result + (butList != null ? butList.hashCode() : 0);
        result = 31 * result + (boxeList != null ? boxeList.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object futurState) {
        if (this == futurState) return true;
        if (futurState == null || getClass() != futurState.getClass()) return false;
        int sumOfEqualNode = 0;

        EtatSokoban thatFutur = (EtatSokoban) futurState;

        if(!thatFutur.player.equals(this.player))return false;

        for (Noeud n : thatFutur.boxeList) {
            for (Noeud n1 : this.boxeList) {
                if (n.equals(n1)) {
                    sumOfEqualNode++;
                }
            }
            if(sumOfEqualNode == 0) return false;
        }
        return sumOfEqualNode == boxeList.size() &&
                thatFutur.player.equals(this.player);
    }


    @Override
    public int compareTo(Etat o) {
        EtatSokoban etat = (EtatSokoban) o;
        return this.f < etat.f ? -1 : this.f == etat.f ? this.equals(etat) ? 1 : -1 : 1;
    }

}
