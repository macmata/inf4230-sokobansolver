/* INF4230 - Intelligence artificielle
 * UQAM / Département d'informatique
 * Automne 2014 / TP1 - Algorithme A*
 * http://ericbeaudry.ca/INF4230/tp1/
 */
package sokoban;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Représente un problème chargé d'un fichier test sokoban??.txt.
 */
public class Probleme {
    public Grille grille;
    public EtatSokoban etatInitial;
    public But but;

    private Probleme() {
    }

    public static Probleme charger(BufferedReader br) throws IOException {
        ArrayList<Noeud> allNoeuds = new ArrayList<>();

        String ligne;
        int lineNumber = 0;
        int maxWidth = 0;

        Probleme probleme = new Probleme();
        probleme.but = new But();
        probleme.etatInitial = new EtatSokoban();

        while ((ligne = br.readLine()) != null && !ligne.isEmpty()) {

            maxWidth = maxWidth < ligne.length() ? ligne.length() : maxWidth;

            for (int x = 0; x < ligne.length(); x++) {
                char c = ligne.charAt(x);
                switch (c) {
                    case ' ':
                        //espace
                        allNoeuds.add(new Noeud(x, lineNumber));
                        break;
                    case '#':
                        //block
                        allNoeuds.add(new Noeud(x, lineNumber, true));
                        break;
                    case '$':
                        //position inital
                        allNoeuds.add(new Noeud(x, lineNumber));
                        probleme.etatInitial.addBox(new Noeud(x, lineNumber));
                        break;
                    case '.':
                        //position final
                        Noeud noeud2 = new Noeud(x, lineNumber);
                        probleme.but.addBut(noeud2);
                        allNoeuds.add(noeud2);
                        break;
                    case '@':
                        //position du joueur
                        Noeud noeud3 = new Noeud(x, lineNumber);
                        allNoeuds.add(noeud3);
                        probleme.etatInitial.player = new Noeud(x, lineNumber);
                    default:
                        //espace
                        allNoeuds.add(new Noeud(x, lineNumber));
                }
            }
            lineNumber++;
        }

        probleme.grille = new Grille(maxWidth, lineNumber);
        probleme.grille.addAll(allNoeuds);
        for (int y = 0; y < probleme.grille.height; y++) {
            for (int x = 0; x < probleme.grille.width; x++) {
                if (probleme.grille.grille[x][y] != null && probleme.grille.grille[x][y].block) {
                    System.out.printf("#");
                } else {
                    System.out.printf(" ");
                }
            }
            System.out.println();
        }
        return probleme;
    }
}
