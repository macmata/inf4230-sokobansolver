/* INF4230 - Intelligence artificielle
 * UQAM / Département d'informatique
 * Automne 2014 / TP1 - Algorithme A*
 * http://ericbeaudry.ca/INF4230/tp1/
 */
package astar;


import java.text.NumberFormat;
import java.util.*;

public class AStar {

    static final NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);

    static {
        nf.setMaximumFractionDigits(1);
    }

    public static List<Action> genererPlan(Monde monde, Etat etatInitial, But but, Heuristique heuristique) {
        long starttime = System.currentTimeMillis();
        PriorityQueue<Etat> open = new PriorityQueue<>(5000,new Fcomparator());
        HashSet<Etat> close = new HashSet<>();
        int noeudAjouter = 0;
        int loop = 0;

        etatInitial.parent = null;
        etatInitial.actionDepuisParent = null;
        etatInitial.h = heuristique.estimerCoutRestant(etatInitial, but);

        open.add(etatInitial);
        while (!open.isEmpty()) {
            loop++;
            Etat currentState = open.poll();

            if (but.butSatisfait(currentState)) {
                List<astar.Action> plan = reconstructPath(currentState);
                long lastDuration = System.currentTimeMillis() - starttime;
                printDebugiInfo(noeudAjouter, loop, plan, lastDuration);
                return plan;
            }
            close.add(currentState);
            //currentState.debug();

            List<Action> listAction = monde.getActions(currentState);


            for (Action futurAction : listAction) {

                Etat futurState = monde.executer(currentState, futurAction);

                Boolean closeL = close.contains(futurState);
                if (closeL) {
                    continue;
                }


                Boolean openListContainFuturState = open.contains(futurState);

                if (!openListContainFuturState) {
                    noeudAjouter++;
                    futurState.g = currentState.g + futurAction.cout;
                    futurState.h = heuristique.estimerCoutRestant(futurState, but);
                    futurState.f = futurState.g + futurState.h;
                    futurState.parent = currentState;
                    futurState.actionDepuisParent = futurAction;
                    open.add(futurState);

                }
            }
        }
        return null;
    }

    private static void printDebugiInfo(int noeudAjouter, int loop, List<Action> plan, long lastDuration) {
        System.out.println("# Nombre d'états visités: " + loop);
        System.out.println("# Nombre d'états générés: " + noeudAjouter);
        System.out.println("# Durée : " + lastDuration + " ms");
        System.out.println("# Coût : " + totalCost(plan));
    }

    static private double totalCost(List<astar.Action> plan) {
        double totalCost = 0;
        for (Action action : plan) {
            totalCost += action.cout;
        }
        return totalCost;
    }

    static List<astar.Action> reconstructPath(Etat state) {
        List<astar.Action> plan = new ArrayList<>();
        Etat currentState = state;
        while (currentState.actionDepuisParent != null) {

            plan.add(currentState.actionDepuisParent);

            currentState = currentState.parent;
        }
        Collections.reverse(plan);
        return plan;
    }

    private static boolean contains(TreeSet<Etat> tree, Etat futurState){
        for(Etat oldState: tree){
            if (oldState.equals(futurState)){
                return true;
            }
        }
        return false;
    }
    public static class Fcomparator implements Comparator<Etat> {

        @Override
        public int compare(Etat o, Etat o2) {
            return o.f < o2.f ? -1 : o.f == o2.f ? 0 : 1;
        }
    }
}
